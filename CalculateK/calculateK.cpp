#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<cmath>
#include<algorithm>
using namespace std;

class StrPositions
{
public:
    string str;
    vector<int> positions;
    StrPositions(string newStr, vector<int> newPositions)
    {
        this->str = newStr;
        this->positions = newPositions;
    }
    bool strIsEqual(string argu_str)
    {
        return str == argu_str;
    }
};

string cypher;//存储密文
const int num_lines = 8;//原始密文有几行
const int kasiskiStartSize = 3;
const int kasiskiEndSize = 5;

vector<StrPositions> v_strPositons;

bool consisted(string argu_str)
{
    for (int i = 0; i < v_strPositons.size(); i++)
    {
        if (v_strPositons[i].str == argu_str)
        {
            return true;
        }
    }
    return false;
}

vector<int> getStrPositions(string argu_str)
{
    int strSize = argu_str.size();
    vector<int> ans;
    for (int i = 0; i < cypher.size(); i++)
    {
        if (cypher[i] == argu_str[0])
        {
            bool findOne = true;
            for (int j = i; j < i + strSize && j<cypher.size(); j++)
            {
                if (cypher[j] != argu_str[j - i])
                {
                    findOne = false;
                    break;
                }
                if (j + 1 >= cypher.size())
                {
                    findOne = false;
                }
            }
            if (findOne)
            {
                ans.push_back(i);
                i += (strSize - 1);
            }
        }
    }
    return ans;
}

void getAllStrPosition(int n)//n>=3
{
    for (int i = 0; i < cypher.size() - n; i++)
    {
        string currStr;
        
        for (int j = i; j < i + n; j++)
        {
            currStr += cypher[j];
        }
        if (consisted(currStr))continue;
        vector<int> newVector = getStrPositions(currStr);
        if (newVector.size() <= 2) continue;
        StrPositions newSP(currStr, newVector);
        v_strPositons.push_back(newSP);
    }
}

int main()
{
    //step1：从文件中读取维吉尼亚密码存到cypher中
    //pwd.txt为课本例1.12的密文，pwd2.txt为作业1.21(b)中的密文

	ifstream file("pwd2.txt");
    
    if (file.is_open()) { 

        for (int i = 0; i < num_lines; i++)
        {
            string currLineStr;
            getline(file, currLineStr);
            cypher += currLineStr;
        }
        file.close(); 
    }
    else {
        std::cout << "无法打开文件" << std::endl;
    }
    
    cout <<"获取到的密文是：" << cypher << endl << endl;

    //step2：Kasiski测试
    cout << "Kasiski测试：" << endl;
    for (int i = kasiskiStartSize; i <= kasiskiStartSize; i++)
    {
        getAllStrPosition(i);
    }

    for (int i = 0; i < v_strPositons.size(); i++)
    {
        cout << v_strPositons[i].str << " ";
        for (int j = 0; j < v_strPositons[i].positions.size(); j++)
        {
            cout << v_strPositons[i].positions[j] << " ";
        }
        cout << endl << "距离:" << endl;
        for (int j = 1; j < v_strPositons[i].positions.size(); j++)
        {
            cout << v_strPositons[i].positions[j]- v_strPositons[i].positions[0] << " ";
        }
        cout << endl;
    }

    //step3：利用重合指数确认结果：
    cout<<endl << "利用重合指数确认结果:" << endl;
    int m;
    vector<string> v_str;
    vector<double> v_Ic;
    while (1)
    {
        v_Ic.clear();
        v_str.clear();
        cout << "请输入m：（输入-1结束这一步骤）" << endl;
        cin >> m;
        if (m == -1) break;
        for (int i = 0; i < m; i++)
        {
            int freq[26];
            for (int j = 0; j < 26; j++)
            {
                freq[j] = 0;
            }
            string newStr;
            int n = 0;
            for (int j = i; j < cypher.size(); j += m)
            {
                newStr += cypher[j];
                n++;
                freq[cypher[j] - 'A']++;
            }
            double newIc = 0.0;
            for (int i = 0; i < 26; i++)
            {
                newIc += freq[i] * (freq[i] - 1);
            }
            newIc /= (n * (n - 1));
            v_Ic.push_back(newIc);
        }
        for (int i = 0; i < v_Ic.size(); i++)
        {
            cout << v_Ic[i] << endl;
        }
    }
    

    //step4：确定K
    cout << "请确定最终的m" << endl;
    cin >> m;
    v_str.clear();
    int freq[100][26];
    for (int i = 0; i < m; i++)//重新设置v_str以及freq
    {
        for (int j = 0; j < 26; j++)
        {
            freq[i][j] = 0;
        }
        string newStr;
        for (int j = i; j < cypher.size(); j += m)
        {
            newStr += cypher[j];
            freq[i][cypher[j] - 'A']++;
        }
    }
    int n = cypher.size() / m;
    double Mg[100][26];
    for (int i = 0; i < 100; i++)
    {
        for (int j = 0; j < 26; j++)
        {
            Mg[i][j] = 0;
        }
    }
    double p[26] = { 0.082, 0.015,0.028,0.043,0.127,0.022,0.020,0.061,0.070,0.002,0.008,0.040,0.024,0.067,0.075,0.019,0.001,0.060,0.063,0.091,0.028,0.010,0.023,0.001,0.020,0.001 };
    for (int i = 0; i < m; i++)//算出所有Mg的值
    {
        for (int g = 0; g < 26; g++)
        {
            for (int j = 0; j < 26; j++)
            {
                Mg[i][g] += p[j] * freq[i][(j + g) % 26];
            }
            Mg[i][g] /= n;
        }
    }
    for (int i = 0; i < m; i++)
    {
        double minDis = 0.0;
        int target = 0;
        for (int j = 0; j < 26; j++)
        {
            cout << Mg[i][j] << " ";
            if (j == 0) 
            {
                minDis = abs(Mg[i][j] - 0.065);
                target = 0;
            }
            else if(abs(Mg[i][j] - 0.065) < minDis)
            {
                minDis = abs(Mg[i][j] - 0.065);
                target = j;
            }
        }
        cout <<"target："<<target << endl;
    }

	return 0;
}