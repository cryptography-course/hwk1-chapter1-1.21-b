#include<iostream>
#include<fstream>
#include<string>
using namespace std;

char transChar(char c, int k)
{
    int tmp = (c - 'A' - k)%26;
    if (tmp < 0) tmp += 26;
    return (char)(tmp+'A');
}

int main()
{
	string cypher;
    const int num_lines = 8;
    ifstream file("pwd2.txt");
    int num_k;
    int k[100];
    cin >> num_k;
    for (int i = 0; i < num_k; i++)
    {
        cin >> k[i];
    }

    if (file.is_open()) {

        for (int i = 0; i < num_lines; i++)
        {
            string currLineStr;
            getline(file, currLineStr);
            cypher += currLineStr;
        }
        file.close();
    }
    else {
        std::cout << "无法打开文件" << std::endl;
    }

    cout << cypher << endl << endl;

    for (int i = 0; i < cypher.size(); )
    {
        for (int j = 0; j < num_k; j++)
        {
            cypher[i] = transChar(cypher[i], k[j]);
            i++;
            if (i >= cypher.size()) break;
        }
    }
    cout << cypher << endl;
}